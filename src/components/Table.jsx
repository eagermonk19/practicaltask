import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";

const Table = ({ shipments }) => {
  return (
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Cargo Type</th>
          <th>Cargo Desc</th>
          <th>Cargo Vol</th>
          <th>Mode</th>
          <th>type</th>
          <th>Dest</th>
          <th>Origin</th>
          <th>Service</th>
          <th>Total</th>
          <th>status</th>
          <th>userid</th>
        </tr>
      </thead>
      <tbody>
        {shipments.length > 0 ? (
          shipments.map((shipment, index) => {
            return (
              <tr key={index}>
                <td>{shipment.name}</td>
                <td>
                  {shipment.cargo.map((cargo,index) => (
                    <div   key={index}>
                      <span>{cargo.type},</span>
                    </div>
                  ))}
                </td>
                <td>
                  {shipment.cargo.map((cargo,index) => (
                    <div   key={index}>
                      <span>{cargo.description},</span>
                    </div>
                  ))}
                </td>
                <td>
                  {shipment.cargo.map((cargo,index) => (
                    <div   key={index}>
                      <span>{cargo.volume},</span>
                    </div>
                  ))}
                </td>
                <td>{shipment.mode}</td>
                <td>{shipment.type}</td>
                <td>{shipment.destination}</td>
                <td>{shipment.origin}</td>
                <td>
                  {shipment.services.map((service,index) => (
                    <div   key={index}>
                      <span>{service.type},</span>
                    </div>
                  ))}
                </td>
                <td>{shipment.total}</td>
                <td>{shipment.status}</td>
                <td>{shipment.userId}</td>
              </tr>
            );
          })
        ) : (
          <tr>
            <td colSpan="5">Loading...</td>
          </tr>
        )}
      </tbody>
    </table>
  );
};
export default Table;
