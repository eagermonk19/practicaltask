import React, { Component } from "react";
import "./App.css";
import Table from "./components/Table";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shipments: []
    };
  }
  componentDidMount() {
    let url = "http://localhost:3001/shipments";
    fetch(url)
      .then(res => res.json())
      .then(data => this.setState({ shipments: data }));
  }

  render() {
    return (
      <div className="App">
        <Table shipments={this.state.shipments} />
      </div>
    );
  }
}

export default App;
